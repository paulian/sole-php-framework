<?php
    Migrate::$migration = ["UserMigration"];

    class UserMigration
    {
        public static function index(){
            Migrate::attrib_table("user");
            Migrate::attrib_string(255);
            Migrate::string("name");
            Migrate::string("address");
            Migrate::string("email");
            Migrate::string("no");
        }
    }
?>